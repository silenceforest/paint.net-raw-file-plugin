﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Reflection;
using PaintDotNet;
using PaintDotNet.IO;

namespace RAWFile
{
    public sealed class RAWFileTypeFactory : IFileTypeFactory
    {
        #region IFileTypeFactory Members

        public FileType[] GetFileTypeInstances()
        {
            return new[] {new RAWFileType()};
        }

        #endregion
    }

    internal class RAWFileType : FileType
    {
        private static string _DcrawFile;

        internal RAWFileType()
            : base(
                "RAW File", FileTypeFlags.SupportsLoading,
                new[]
                    {
                        ".arw", ".cf2", ".cr2", ".crw", ".dng", ".erf", ".mef", ".mrw", ".nef", ".orf", ".pef", ".raf",
                        ".raw", ".sr2", ".x3f"
                    })
        {
        }

        private static Process CreateRawProcess(string parameter)
        {
            var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("RAWFile.dcraw.exe");
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var file = folder + "\\dcraw.exe";

            if (_DcrawFile == null)
            {
                try
                {
                    using (var writer = new BinaryWriter(File.Create(file)))
                    {
                        var size = (int) stream.Length;
                        var bytes = new byte[size];
                        stream.Read(bytes, 0, size);
                        writer.Write(bytes, 0, size);
                    }
                }
                catch(Exception ex)
                {
                    throw ex;
                }
            }

            _DcrawFile = file;

            var process = new Process
                              {
                                  StartInfo =
                                      {
                                          FileName = file,
                                          Arguments = "-w -q 3 -c -T \"" + parameter + "\""
                                      }
                              };

            return process;
        }

        private static Bitmap LoadRAW(string file)
        {
            var process = CreateRawProcess(file);
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.CreateNoWindow = true;
            process.StartInfo.RedirectStandardOutput = true;
            process.StartInfo.RedirectStandardError = true;
            process.Start();
            var bmp = new Bitmap(process.StandardOutput.BaseStream);
            process.WaitForExit();
            var c = process.ExitCode;

            return bmp;
        }

        private static Bitmap LoadRAW(Stream input)
        {
            var folder = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
            var file = folder + "\\PaintNET.raw";
            using (var writer = new BinaryWriter(File.Create(file)))
            {
                var size = (int) input.Length;
                var bytes = new byte[size];
                input.Read(bytes, 0, size);
                writer.Write(bytes, 0, size);
            }
            return LoadRAW(file);
        }

        protected override Document OnLoad(Stream input)
        {
            try
            {
                var ss = (SiphonStream) input;
                var fi = ss.GetType().GetField("stream", BindingFlags.NonPublic | BindingFlags.Instance);
                var fs = fi.GetValue(ss) as FileStream;
                using (Image image = LoadRAW(fs.Name))
                {
                    return Document.FromImage(image);
                }
            }
            catch
            {
                using (Image image = LoadRAW(input))
                {
                    return Document.FromImage(image);
                }
            }
        }
    }
}