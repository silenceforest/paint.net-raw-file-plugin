Paint.NET RAW File plugin
---------------

Fork Paint.NET RAW File plugin from [http://forums.getpaint.net/index.php?/topic/15611-raw-file/](http://forums.getpaint.net/index.php?/topic/15611-raw-file/)
Bugs where fixed. This is working version.

Available [here](https://bitbucket.org/silenceforest/paint.net-raw-file-plugin/downloads/RAWFile.zip)

You should:

  - copy RAWFile.dll into the folder \Paint.NET\FileTypes\
  - and place dcraw.exe to \Paint.NET